using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using TestAPI;
using TestAPI.Controllers;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var logger = Substitute.For<ILogger<WeatherForecastController>>();
            var ctrl = new WeatherForecastController(logger);

            IEnumerable<WeatherForecast> getResult = ctrl.Get();
            Assert.That(getResult, Is.InstanceOf<IEnumerable<WeatherForecast>>());
        }
    }
}